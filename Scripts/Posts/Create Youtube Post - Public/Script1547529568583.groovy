import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Add Post/Page_Salomondrin/a (1) (1)'))

WebUI.setText(findTestObject('Object Repository/Add Post/Page_Add new post/input_Import_youtube_url (1)'), 'https://www.youtube.com/watch?v=k5sd6OdTMCY')

WebUI.click(findTestObject('Object Repository/Add Post/Page_Add new post/app-localize_Import (1)'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Add Post/Page_Add new post/input_concat(This post is avai'), 0)

WebUI.click(findTestObject('Add Post/Page_Add new post/input_concat(This post is avai'))

WebUI.scrollToElement(findTestObject('Add Post/Page_Add new post/button_Upload'), 0)

WebUI.click(findTestObject('Object Repository/Add Post/Page_Add new post/button_Upload (1)'))

