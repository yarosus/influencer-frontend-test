<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Basic</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ee1d9ec4-8c53-4eac-87fa-68309fb3400b</testSuiteGuid>
   <testCaseLink>
      <guid>e41ad3fe-a6c9-4ed2-b3a0-2674a321e613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7768720e-e75c-41bb-aafc-adaec380b95f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Posts/Create Youtube Post - Public</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30aa4cc7-3bd2-4e18-ad29-931bfd1b021b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Comments/Create First Comment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0274255b-dc39-41fa-a1ff-0e28761d2536</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Comments/Edit First Comment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dc34f75-410e-4ea9-9f81-109c1ffc20c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Comments/Delete First Comment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dad941b-75ad-440f-8e0f-3aaf37705223</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Posts/Edit Created Post - Update Post Title</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c972f80b-b039-4841-9b27-8723d166b354</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Posts/Delete Created Post from post view</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
