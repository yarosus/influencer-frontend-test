import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Basic')

suiteProperties.put('name', 'Basic')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\WD\\influencer-frontend-test\\Reports\\Basic\\20190117_214923\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Basic', suiteProperties, [new TestCaseBinding('Test Cases/Login', 'Test Cases/Login',  null), new TestCaseBinding('Test Cases/Posts/Create Youtube Post - Public', 'Test Cases/Posts/Create Youtube Post - Public',  null), new TestCaseBinding('Test Cases/Comments/Create First Comment', 'Test Cases/Comments/Create First Comment',  null), new TestCaseBinding('Test Cases/Comments/Edit First Comment', 'Test Cases/Comments/Edit First Comment',  null), new TestCaseBinding('Test Cases/Comments/Delete First Comment', 'Test Cases/Comments/Delete First Comment',  null), new TestCaseBinding('Test Cases/Posts/Edit Created Post - Update Post Title', 'Test Cases/Posts/Edit Created Post - Update Post Title',  null), new TestCaseBinding('Test Cases/Posts/Delete Created Post from post view', 'Test Cases/Posts/Delete Created Post from post view',  null)])
