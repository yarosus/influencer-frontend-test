<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>app-localize_This post is avai</name>
   <tag></tag>
   <elementGuidId>d2e59057-a61d-421b-b61d-28ab8baa2bd9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('This post is available for everyone, but it', &quot;'&quot;, 's exclusive')])[1]/following::app-localize[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>app-localize</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This post is available for everyone</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-add-post[1]/main[@class=&quot;Upload&quot;]/div[@class=&quot;body-section&quot;]/div[@class=&quot;body-container&quot;]/form[@class=&quot;upload ng-invalid ng-touched ng-dirty&quot;]/fieldset[@class=&quot;group options&quot;]/div[@class=&quot;form-field field-14 radio&quot;]/label[1]/app-localize[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('This post is available for everyone, but it', &quot;'&quot;, 's exclusive')])[1]/following::app-localize[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This post is only available for paid members'])[1]/following::app-localize[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Upload'])[2]/preceding::app-localize[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//fieldset[5]/div[3]/label/app-localize</value>
   </webElementXpaths>
</WebElementEntity>
