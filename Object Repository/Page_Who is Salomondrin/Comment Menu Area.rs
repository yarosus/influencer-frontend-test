<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Comment Menu Area</name>
   <tag></tag>
   <elementGuidId>d52a7ad8-b558-45ca-8d4c-f151be582376</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>m</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>meta</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>•Reply•0 likes•a few seconds agoEditDelete</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-post[1]/main[@class=&quot;Post&quot;]/div[2]/app-comments[1]/div[@class=&quot;body-section comments&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;feed&quot;]/app-comment[1]/div[@class=&quot;comment&quot;]/div[@class=&quot;details&quot;]/div[@class=&quot;meta&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='yaroparktest'])[4]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Popular'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//app-comment/div/div/div[3]</value>
   </webElementXpaths>
</WebElementEntity>
