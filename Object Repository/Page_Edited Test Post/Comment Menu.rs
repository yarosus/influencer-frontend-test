<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Comment Menu</name>
   <tag></tag>
   <elementGuidId>dabf96fa-f811-4039-8362-98753350a4b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='a few seconds ago'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[1]/app-root[1]/app-post[1]/main[@class=&quot;Post&quot;]/div[2]/app-comments[1]/div[@class=&quot;body-section comments&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;feed&quot;]/app-comment[1]/div[@class=&quot;comment&quot;]/div[@class=&quot;details&quot;]/div[@class=&quot;meta&quot;]/div[@class=&quot;item ellipsis&quot;]/a[@class=&quot;dropdown-btn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-btn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-post[1]/main[@class=&quot;Post&quot;]/div[2]/app-comments[1]/div[@class=&quot;body-section comments&quot;]/div[@class=&quot;body-container&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;feed&quot;]/app-comment[1]/div[@class=&quot;comment&quot;]/div[@class=&quot;details&quot;]/div[@class=&quot;meta&quot;]/div[@class=&quot;item ellipsis&quot;]/a[@class=&quot;dropdown-btn&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='a few seconds ago'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='•'])[3]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[2]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[3]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//app-comment/div/div/div[3]/div[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
